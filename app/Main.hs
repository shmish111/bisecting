module Main where

import qualified TestRunSimulator
import System.Environment (getArgs)

main :: IO ()
main = do
  [filePath, sampleSizeArg] <- getArgs
  let sampleSize = read sampleSizeArg
  TestRunSimulator.run filePath sampleSize
