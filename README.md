# bisecting

A small project to simulate test pipeline batching

## Usage

```
stack build
stack exec bisecting "test.svg" 10000
```
