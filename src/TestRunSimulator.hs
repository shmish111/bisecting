module TestRunSimulator
    ( run
    ) where

import           Control.Monad                             (forM)
import           Control.Monad.Random                      (evalRandIO)
import Data.List (sort)
import           Data.Monoid                               ((<>))
import           Graphics.Rendering.Chart.Backend.Diagrams (FileOptions, toFile)
import           Graphics.Rendering.Chart.Easy             (EC (..),
                                                            Layout (..),
                                                            axis_grid,
                                                            axis_labels,
                                                            axis_show_labels,
                                                            axis_show_line,
                                                            axis_show_ticks,
                                                            axis_ticks, black,
                                                            def, laxis_override,
                                                            laxis_title,
                                                            layout_background,
                                                            layout_bottom_axis_visibility,
                                                            layout_foreground,
                                                            layout_left_axis_visibility,
                                                            layout_legend,
                                                            layout_title,
                                                            layout_x_axis, line,
                                                            opaque, plot,
                                                            solidFillStyle,
                                                            white, (.=), (.~))
import           System.Random.Shuffle                     (shuffleM)

halfList :: [a] -> ([a], [a])
halfList xs = splitAt (length xs `div` 2) xs

mkTests :: [Bool] -> Int
mkTests [] = 0
mkTests (True:False:[]) = 1
mkTests (False:True:[]) = 1
mkTests (x:[]) = 1
mkTests xs = case (False `elem` xs) of
         False -> 1
         True -> let (as, bs) = halfList xs
                     resA = mkTests as
                     resB = mkTests bs
                  in
                    1 + resA + resB

genTests :: Int -> Int -> IO [Bool]
genTests ts fs = evalRandIO . shuffleM $ (replicate ts True) <> (replicate fs False)

runTests :: Int -> Int -> IO Int
runTests ts fs = fmap mkTests $ genTests ts fs

mkTestSpecs :: Int -> [(Int, Int, Int)]
mkTestSpecs sampleSize = [g x | x <- [0..100]]
  where
    f :: Int -> Int
    f n = floor ((fromIntegral n / 100) * (fromIntegral sampleSize))
    g n = (n, sampleSize - (f n), (f n))

mkChart :: Int -> [(Int, Int)] -> EC (Layout Int Int) ()
mkChart sampleSize ps = do
  let cross = closest sampleSize $ fmap snd ps
  layout_title .= "Test Runs Required for " <> show sampleSize <> " tests"
  layout_x_axis . laxis_title .= "Probability of test failing as a percentage"
  layout_background .= solidFillStyle (opaque white)
  layout_foreground .= (opaque black)
  layout_legend .= Nothing
  layout_bottom_axis_visibility . axis_show_ticks .= False
  layout_left_axis_visibility . axis_show_line .= True
  let grid = (fst cross) : [5,10..100]
      label n = (n, show n)
      labels = fmap label grid
  layout_x_axis . laxis_override .= (\ad -> axis_labels .~ [labels] $
                                            axis_grid .~ grid $ ad)
  plot (line "a" [ps])
  plot (line "b" [[(n, sampleSize) :: (Int, Int) | n <- [0..100]]])

closest :: Ord a => a -> [a] -> (Int, a)
closest n xs = let sorted = zip [0..] (sort xs) in
  foldl choose (head sorted) sorted
  where
    choose (i, current) (j, next)
      | current < n && n < next = (j, next)
      | current < n = (j, next)
      | otherwise = (i, current)

run :: FilePath -> Int -> IO ()
run filePath sampleSize = do
  let batches = mkTestSpecs sampleSize
  rs <- forM batches $ \(n, ts, fs) -> do
    res <- runTests ts fs
    pure (n, res)
  toFile def filePath $ mkChart sampleSize rs
